package com.partner.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.partner.qa.base.TestBase;
import com.partner.qa.util.TestUtil;

public class Ratecardpage extends TestBase {

	@FindBy(xpath = "//div[@class='mt-2 font-semibold pr-3 mt-5 text-smh'and contains(text(),'Showing are all our test pricing')]")
	WebElement testpricingtitle;

	public Ratecardpage() {
		PageFactory.initElements(Driver, this);
	}

	public void clickontest(String testname) {
		WebElement name = Driver.findElement(By.xpath("//div[@class='card-body']//p[contains(text(),'" + testname + "')]"));
		TestUtil.scrollintoview(name);
		name.click();

	}

	public boolean ratecardtitle() {
		return testpricingtitle.isDisplayed();
	}
}
