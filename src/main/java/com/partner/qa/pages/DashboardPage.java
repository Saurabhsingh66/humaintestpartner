package com.partner.qa.pages;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.partner.qa.base.TestBase;
import com.partner.qa.util.TestUtil;

public class DashboardPage extends TestBase {

	String Test = prop.getProperty("testname");
    @CacheLookup
	@FindBy(xpath = "//div[contains(text(),'saksham')]")
	WebElement username;
    @CacheLookup
	@FindBy(xpath = "//div[contains(text(),'Enter Patient Information')]")
	WebElement verifyaddpatientscreen;
    @CacheLookup
	@FindBy(xpath = "//div[contains(text(),'Create a new  Booking')and @class='left-part text-primary font-semibold py-4 px-6  lg:px-4  text-sm rounded-l sm:p-0 sm:hidden']")
	WebElement Createbutton;
    @CacheLookup
	@FindBy(name = "patientName")
	WebElement patientname;
    @CacheLookup
	@FindBy(name = "age")
	WebElement age;
  
	@FindBy(name = "externalPatientID")
	WebElement externalPatientID;
    
	@FindBy(xpath = "//button[contains(text(),'Proceed')]")
	WebElement Proceedbtn1;
 
	@FindBy(xpath = "//div[contains(text(),'Proceed')]")
	WebElement Proceedbtn2;
   
	@FindBy(xpath = "//div[contains(text(),'Select Test/Health Package')]")
	WebElement selectTestTitle;
    
	@FindBy(xpath = "//p[text()='Malaria']//parent::div[@class='left-container cursor-pointer']//following-sibling::div[@class='right-container flex justify-between items-center']//img[@class='img-responsive']")
	WebElement clickOnAddtest;
   
	@FindBy(xpath = "//span[contains(text(),'Great!')]//parent::div")
	WebElement bookingconfirmedtitle;
    
	@FindBy(xpath = "//div[@class='card-body']//p[contains(text(),'Swine Flu Test')]")
	WebElement findtest;
    
	@FindBy(xpath = "//div[@class='flex justify-between w-full sm:overflow-auto option-container']//div[1]")
	WebElement collectsample;
    
	@FindBy(xpath = "//div[@class='flex justify-between w-full sm:overflow-auto option-container']//div[2]")
	WebElement callphelebotmist;
  
	@FindBy(xpath = "//div[@class='flex justify-between w-full sm:overflow-auto option-container']//div[3]")
	WebElement sendPhelobotmist;
    
	@FindBy(xpath = "//span[contains(text(),'0/1')and @class='text-green text-xl font-bold p-2 sm:text-base']")
	WebElement verify1testadded;
    
	@FindBy(xpath = "//div[span=*]//following-sibling::div[@class='right-container block justify-start items-center w-1/2 sm:w-full sm:mt-3']//input")
	WebElement addbarcodenumber;
   
	@FindBy(xpath ="//div[span=*]//following-sibling::div[@class='right-container block justify-start items-center w-1/2 sm:w-full sm:mt-3']//span")
	WebElement Plusiconbarcode;
    
	@FindBy(xpath="//p[text()='Blood test']//parent::div[@class='left-container cursor-pointer']//following-sibling::div[@class='right-container flex justify-between items-center']//div[@class]")
	WebElement testAmount;
	
   
	@FindBy(xpath = "//div[@class='flex items-center justify-between px-10  mt-6 mb-4 sm:px-0 sm:my-3']//div[contains(text(),'1')]")
	WebElement testnumberselected;

	@FindBy(xpath = "//div[contains(text(),'Proceed')]//parent::div")
	WebElement procedbuttoncallphelobotmist;

	@FindBy(xpath = "//div[contains(text(),'Proceed')]")
	WebElement procedbuttoncallphelobotmist2;
	
	@FindBy(xpath = "//input[@placeholder='Enter Location']")
	WebElement enterlocation;

	@FindBy(xpath = "//input[@placeholder='Enter House number, street name, landmark']")
	WebElement housenumber;

	@FindBy(xpath = "//input[@placeholder='Enter phone number']")
	WebElement patientphonenumber;

	@FindBy(xpath = "//div[@class='nav-div  Rate Card']")
	WebElement ratecardtab;

	public DashboardPage() {
		PageFactory.initElements(Driver, this);
	}

	public void selectgender(String gender) {
		// Driver.findElement(By.xpath("//div[contains(text(),'"+gender+"']//parent::div[@class=\"theme-container
		// flex justify-between mb-8\"]")).click();
		// Driver.findElement(By.xpath("//*[@id=\"app\"]/div[1]/div/div/div/div[2]/div/div/div/div[2]/div/div/div[4]/div[2]"));
		Driver.findElement(By.xpath("//*[contains(text(),'Other')]")).click();

	}

	public void addtest(String testname) {
		WebElement name = Driver
				.findElement(By.xpath("//div[@class='card-body']//p[contains(text(),'" + testname + "')]"));
		TestUtil.visibilitywait(name);
		TestUtil.scrollintoview(name);
		WebElement Addicon = Driver.findElement(By.xpath("//p[text()='" + testname
				+ "']//parent::div[@class='left-container cursor-pointer']//following-sibling::div[@class='right-container flex justify-between items-center']//img[@class='img-responsive']"));
		TestUtil.visibilitywait(Addicon);
		Addicon.click();
		
	}

	public void testamount(String Testname) {
		WebElement amount = Driver.findElement(By.xpath("//p[text()='Blood test']//parent::div[@class='left-container cursor-pointer']//following-sibling::div[@class='right-container flex justify-between items-center']//div[@class]"));
     String price = 		amount.getText();
	 System.out.println("price of test is " + price);
	  
	}
	
	public boolean addPatient_collectsample() throws Exception {
		TestUtil.visibilitywait(Createbutton);
		Thread.sleep(3000);
		TestUtil.visibilitywait(Createbutton);
		Createbutton.click();
		patientname.sendKeys("Collect sample");
		age.sendKeys("34");
		this.selectgender("Female");
		externalPatientID.sendKeys("Selenium at work ");

		
		TestUtil.visibilitywait(Proceedbtn1);
		Proceedbtn1.click();

		System.out.println("search test");
		
		Thread.sleep(5000);  
		this.addtest(Test);

		// TestUtil.scrollintoview(findtest);
		Thread.sleep(1000);
		System.out.println("complete");
		Thread.sleep(2000);
		TestUtil.visibilitywait(Proceedbtn2);
		Proceedbtn2.click();
		TestUtil.visibilitywait(collectsample);
		collectsample.click();
	    
		
		System.out.println("verify 1 test");
		TestUtil.visibilitywait(verify1testadded);
		System.out.println("number");
		
		String number = verify1testadded.getText();
		System.out.println(number);
		if (number.equals("0/1")) {

			assertTrue(true);
		} else {
			throw new Exception("One test barcode should be added only");
		}
		addbarcodenumber.sendKeys("13123123213");
		Plusiconbarcode.click();
		
		Proceedbtn2.click();
        Thread.sleep(1000);
		
		
		TestUtil.visibilitywait(bookingconfirmedtitle);
		
		return bookingconfirmedtitle.isDisplayed();
	}

	public boolean addPatient_callphlebotmist() throws Exception {
		Thread.sleep(2000);
		Createbutton.click();
		
		patientname.sendKeys("Call phlebotmist");
		age.sendKeys("56");
		this.selectgender("Female");
		externalPatientID.sendKeys("selnium at work");

		Proceedbtn1.click();

		System.out.println("search test");

		this.addtest(Test);

		System.out.println("complete");

		TestUtil.visibilitywait(Proceedbtn2);
		Proceedbtn2.click();
		Thread.sleep(3000);
		TestUtil.visibilitywait(callphelebotmist);
		
		callphelebotmist.click();

		TestUtil.visibilitywait(testnumberselected);
		String num = testnumberselected.getText();
		System.out.println(num);
		Assert.assertTrue(testnumberselected.getText().contains("1"));
		
		Thread.sleep(1000);
	
		TestUtil.visibilitywait(procedbuttoncallphelobotmist2);
		procedbuttoncallphelobotmist2.click();
		System.out.println("clicked");
		Thread.sleep(1000);
		TestUtil.visibilitywait(bookingconfirmedtitle);

		return bookingconfirmedtitle.isDisplayed();
	}

	public boolean addPatient_sendphlebotmist_toPatient() throws Exception {
		Thread.sleep(4000);
		TestUtil.visibilitywait(Createbutton);
		Createbutton.click();
		patientname.sendKeys("Send Phlebotmist");
		age.sendKeys("34");
		this.selectgender("Female");
		externalPatientID.sendKeys("Selenium on work");

		Proceedbtn1.click();

		System.out.println("search test");
		

		this.addtest(Test);
		this.testamount(Test);

		// TestUtil.scrollintoview(findtest);
		Thread.sleep(2000);
		System.out.println("complete");
		Thread.sleep(5000);
		Proceedbtn2.click();
		TestUtil.visibilitywait(sendPhelobotmist);
		sendPhelobotmist.click();
		Thread.sleep(5000);
		enterlocation.sendKeys("mahipal pur");
		housenumber.sendKeys("140");
		patientphonenumber.sendKeys("9898989811");
		Thread.sleep(1000);
		System.out.println("now going to click on proceed");
		
		TestUtil.visibilitywait(procedbuttoncallphelobotmist);
		procedbuttoncallphelobotmist.click();
		System.out.println("clicked");
		Thread.sleep(1000);
		TestUtil.visibilitywait(bookingconfirmedtitle);
		return bookingconfirmedtitle.isDisplayed(); 
	}

	public Ratecardpage clickonRateCard() throws InterruptedException {
		
        Thread.sleep(3000);
		ratecardtab.click();
		return new Ratecardpage();
	}

}
