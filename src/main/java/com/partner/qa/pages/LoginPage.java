package com.partner.qa.pages;

import static org.testng.Assert.assertTrue;

import org.apache.commons.lang3.BooleanUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.partner.qa.base.TestBase;
import com.partner.qa.util.TestUtil;

public class LoginPage extends TestBase {
	
	

	@FindBy(xpath = "//div[contains(text(),'Log In here')]")
	WebElement logintitle;
	
	
	@FindBy(name = "email")
	WebElement email;

	@FindBy(name = "password")
	WebElement password;

	@FindBy(xpath = "//button[contains(text(),'Sign In')]")
	WebElement loginbtn;

	@FindBy(xpath = "//h2[text()='Error']")
	WebElement alertbox;

	@FindBy(xpath = "//button[contains(text(),'Register')]")
	WebElement registerbtn;

	@FindBy(xpath = "//span[contains(text(),'Please enter a valid email') and @class='error']")
	WebElement invalidemailalert;

	@FindBy(xpath = "//span[contains(text(),'Please enter a valid  password') and @class ='error']")
	WebElement invalidpasswordalert;

	
	@FindBy(xpath="//img[@class='h-3 w-3 mr-1']")
	WebElement Logout;
	
	public LoginPage() {

		PageFactory.initElements(Driver, this);

	}
	

	
	

	public boolean verifylogintitle() {
		return logintitle.isDisplayed();
	}
	
	public boolean verifylogoutbtn() {
		return Logout.isDisplayed();
	}

	public boolean verifyInvalidEmailAlert() {
		return invalidemailalert.isDisplayed();
	}

	public boolean verifyInvalidPassword() {
		return invalidpasswordalert.isDisplayed();
	}

	public boolean verfiyRegiterButton() {
		return registerbtn.isDisplayed();
	}

	public boolean verify_Invalid_Login_Alert() {
		email.sendKeys(prop.getProperty("invalidusername"));
		password.sendKeys(prop.getProperty("invalidpassword"));
		loginbtn.click();
		TestUtil.visibilitywait(alertbox);
		TestUtil.takescreen(Driver, "logout");
		return alertbox.isDisplayed();

	}
	
	public boolean  verifyloginlogout() throws Exception {
		email.sendKeys(prop.getProperty("username"));
		password.sendKeys(prop.getProperty("password"));
		loginbtn.click();
		Thread.sleep(2000);
	    TestUtil.visibilitywait(Logout);
	    Logout.click();
	    
	    return loginbtn.isDisplayed();
	    
	}

	public DashboardPage partnerlogin() {
		email.sendKeys(prop.getProperty("username"));
		password.sendKeys(prop.getProperty("password"));
		loginbtn.click();
		return new DashboardPage();

	}

}
