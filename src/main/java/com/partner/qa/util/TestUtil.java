package com.partner.qa.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.partner.qa.base.TestBase;



public class TestUtil extends TestBase {

	private static final String File = null;
	public static long implicit_waits = 10;
	public static long page_load_timeout = 15;
	 
	
	
	
	 

	public static void scrollintoview(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) Driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public static void normalscroll() {
		JavascriptExecutor je = (JavascriptExecutor) Driver;
		je.executeScript("window.scrollBy(0,1000)");
	}

	public static void scroll(WebElement element) throws Exception {
		Point hoverItem = Driver.findElement(By.xpath("element")).getLocation();
		((JavascriptExecutor) Driver).executeScript("return window.title;");
		Thread.sleep(6000);
		((JavascriptExecutor) Driver).executeScript("window.scrollBy(0," + (hoverItem.getY()) + ");");
	}

	public static void visibilitywait(WebElement element) {
		WebDriverWait wait = new WebDriverWait(Driver, 12);
		wait.until(ExpectedConditions.elementToBeClickable(element));

	}

	public static String takescreen(WebDriver Driver , String name) {
		String finaldestination = null;
		
	TakesScreenshot scrShot =(TakesScreenshot)Driver;

		File scrFile = 	scrShot.getScreenshotAs(OutputType.FILE); 
		try {
			 finaldestination = currentdir + "\\SCREENSHOTS\\"+name + System.currentTimeMillis() + ".png";
			File destination = new File(finaldestination);
			
			
		    FileUtils.copyFile(scrFile, destination);
			//FileUtils.copyFile(scrFile, new File(("C:\\Users\\Saurabh\\eclipse-workspace\\freecrmnew\\target"+filename+".png")));
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return finaldestination;
	
	}
	
	
	
	
	
	
	
	
	
	
	
}
