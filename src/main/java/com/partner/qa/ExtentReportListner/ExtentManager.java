package com.partner.qa.ExtentReportListner;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentManager {
	 
	    private static ExtentReports extent;
	   
	    private static String reportFileName = "ExtentReports-Version3-Test-Automaton-Report.html";
	   
	    private static String windowsPath = System.getProperty("user.dir")+ "\\TestReport(author-saurabh).html";
	 public static String fileName = "Automation Script(Author- saurabh)";
	 
	 
	    public static ExtentReports getInstance() {
	        if (extent == null)
	            createInstance();
	        return extent;
	    }
	 
	    //Create an extent report instance
	    public static ExtentReports createInstance() {
	       
	        
	        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(windowsPath);
	        htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
	        htmlReporter.config().setChartVisibilityOnOpen(true);
	        htmlReporter.config().setTheme(Theme.STANDARD);
	        htmlReporter.config().setDocumentTitle("Author- saurabh bhandari");
	        htmlReporter.config().setEncoding("utf-8");
	        htmlReporter.config().setReportName(fileName);
	        htmlReporter.getExceptionContextInfo();
	       
	        
	        extent = new ExtentReports();
	        extent.attachReporter(htmlReporter);
	 
	        return extent;
	    }
	    }
	 
	  