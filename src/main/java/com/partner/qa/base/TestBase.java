  package com.partner.qa.base;

import java.io.FileInputStream;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.partner.qa.util.TestUtil;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {

	public static Properties prop;
	public static WebDriver Driver;
	protected static String currentdir = System.getProperty("user.dir");

	public TestBase() {

		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					currentdir + "\\src\\main\\java\\com\\partner\\qa\\config\\config.properties");
			prop.load(ip);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void initialization() {

		String browsername = prop.getProperty("browser");
		if (browsername.equalsIgnoreCase("chrome")) {
			
	
			// System.setProperty("webdriver.chrome.driver",
			// "S:\\Cucumber\\chromedriver.exe");
			// System.setProperty("webdriver.chrome.driver",
			// "C:\\Users\\Saurabh\\eclipse-workspace\\HumainTestPartner\\src\\main\\java\\com\\partner\\qa\\config\\chromedriver.exe");
			System.setProperty("webdriver.chrome.driver",
					currentdir + "\\src\\main\\java\\com\\partner\\qa\\config\\chromedriver.exe");
			Driver = new ChromeDriver();
			Driver.get(prop.getProperty("url"));
			
		} 
		else if (browsername.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", currentdir +"\\src\\main\\java\\com\\partner\\qa\\config\\geckodriver.exe");
			Driver = new FirefoxDriver();
			Driver.get(prop.getProperty("url"));
		}
		
		
		else if(browsername.equalsIgnoreCase("edge")) {
			WebDriverManager.iedriver().setup();
		
			Driver = new InternetExplorerDriver();
			Driver.get("gmail.com");
		}
		else if(browsername.equalsIgnoreCase("html")) {
			// Declaring and initialising the HtmlUnitWebDriver
			//HtmlUnitDriver Driver = new HtmlUnitDriver(BrowserVersion.CHROME);
	
		}
		
		
		
		Driver.manage().window().maximize();
		Driver.manage().deleteAllCookies();
		Driver.manage().timeouts().implicitlyWait(TestUtil.implicit_waits, TimeUnit.SECONDS);
		Driver.manage().timeouts().pageLoadTimeout(TestUtil.page_load_timeout, TimeUnit.SECONDS);

	}

	
}
