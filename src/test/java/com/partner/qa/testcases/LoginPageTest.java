package com.partner.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.partner.qa.base.TestBase;
import com.partner.qa.pages.DashboardPage;
import com.partner.qa.pages.LoginPage;
import com.partner.qa.util.TestUtil;

import Analyzer.RetryAnalyzer;

public class LoginPageTest extends TestBase {

	LoginPage loginpage;
	DashboardPage dashboardpage;

	LoginPageTest() {
		super();
	}

	@BeforeMethod
	public void setup() {
		System.out.println("first initialization");
		initialization();
		System.out.println("initialization complete");
		loginpage = new LoginPage();

	}

	@AfterMethod
	public void teardown() {
		System.out.println("close");
		Driver.quit();
	}

	@Test
	public void verifylogintitle() {
		System.out.println("first method");
		TestUtil.takescreen(Driver, "test");
		
		Assert.assertTrue(loginpage.verifylogintitle());

	}
 
	
	@Test
	public void verifypartnerlogintest() {
		System.out.println("second ");
		dashboardpage = loginpage.partnerlogin();

	}

	@Test
	public void inValidloginTest() {
		Assert.assertTrue(loginpage.verify_Invalid_Login_Alert());
	}

	
	@Test
	public void Logout() throws Exception {
		Assert.assertTrue(loginpage.verifyloginlogout());
	}
	

}
