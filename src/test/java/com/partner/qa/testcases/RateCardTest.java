package com.partner.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.partner.qa.base.TestBase;
import com.partner.qa.pages.DashboardPage;
import com.partner.qa.pages.LoginPage;
import com.partner.qa.pages.Ratecardpage;

public class RateCardTest extends TestBase {

	LoginPage loginpage;
	DashboardPage dashboard;
	Ratecardpage ratecardpage;

	public RateCardTest() {
		super();
	}

	@BeforeMethod()
	public void setUp() throws Exception {
		initialization();
		loginpage = new LoginPage();
		dashboard = loginpage.partnerlogin();
		ratecardpage = dashboard.clickonRateCard();

	}
 
	@AfterMethod
	public void teardown() {
		Driver.quit();
	}

	@Test
	public void verify_Ratecardpage() {
		Assert.assertTrue(ratecardpage.ratecardtitle(), "Pass");
	}

	@Test
	public void test_detail() throws Exception {
		ratecardpage.clickontest(prop.getProperty("testname"));
		Thread.sleep(4000);
	}

}
