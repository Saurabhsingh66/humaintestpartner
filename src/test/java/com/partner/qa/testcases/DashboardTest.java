package com.partner.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.partner.qa.base.TestBase;
import com.partner.qa.pages.DashboardPage;
import com.partner.qa.pages.LoginPage;

public class DashboardTest extends TestBase {
	DashboardPage dashboardpage;
	LoginPage loginpage;

	public DashboardTest() {
		super();
	}

	@BeforeMethod
	public void setup() {
		initialization();
		loginpage = new LoginPage();
		dashboardpage = new DashboardPage();
		dashboardpage = loginpage.partnerlogin();

	}

	@AfterMethod()
	public void teardown() {

		Driver.quit();
	}

	@Test
	public void AddPatient_collectsample() throws Exception {
		Assert.assertTrue(dashboardpage.addPatient_collectsample());

	} 

	@Test
	public void Addpateint_callPhlebotmist() throws Exception {
		Assert.assertTrue(dashboardpage.addPatient_callphlebotmist());
	}

	@Test
	public void Addpateint_Sendphlebotmist() throws Exception {
		Assert.assertTrue(dashboardpage.addPatient_sendphlebotmist_toPatient());
	}

}
